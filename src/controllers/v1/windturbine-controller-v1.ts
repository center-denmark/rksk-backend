import { Router, IRouter } from 'express';
import { IController } from "../../interfaces/controller";

import sqlite3 from 'sqlite3'

export class WindTurbineControllerV1 implements IController {

    private static readonly DB_NAME:string = 'db/turbines.db'

    private readonly router = Router()

    constructor() {
        this.router.get('/', (req, res) => {
            let db = new sqlite3.Database(WindTurbineControllerV1.DB_NAME)

            db.all('SELECT * FROM wind_turbines', [], (err, rows) => {
                console.log(`Returned ${rows.length} rows`)
                res.json(rows)
            })

            db.close()
        })

        this.router.get('/:municipality_no', (req, res) => {
            let db = new sqlite3.Database(WindTurbineControllerV1.DB_NAME)

            db.all('SELECT * FROM wind_turbines WHERE municipality_no = ?', [Number(req.params.municipality_no)], (err, rows) => {
                console.log(`Returned ${rows.length} rows`)
                res.json(rows)
            })

            db.close()
        })
    }

    getRouter(): IRouter {
        return this.router
    }
}