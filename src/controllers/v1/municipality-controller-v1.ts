import { Router, IRouter } from 'express';
import { IController } from "../../interfaces/controller";

import sqlite3 from 'sqlite3'

export class MunicipalitiesControllerV1 implements IController {

    private static readonly DB_NAME:string = 'db/turbines.db'

    private readonly router = Router()

    constructor() {
        this.router.get('/', (req, res) => {
            let db = new sqlite3.Database(MunicipalitiesControllerV1.DB_NAME)

            db.all('SELECT municipality_no, municipality FROM wind_turbines GROUP BY municipality_no', [], (err, rows) => {
                console.log(`Returned ${rows.length} rows`)
                res.json(rows)
            })
        })
    }

    getRouter(): IRouter {
        return this.router
    }
}