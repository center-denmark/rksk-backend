import { IRouter, Router } from "express";
import { IController } from "../../interfaces/controller";

import sqlite3 from 'sqlite3'

export class LayersControllerV1 implements IController {

    private static readonly DB_NAME:string = 'db/rksk.db'

    private readonly router = Router()

    constructor() {
        this.router.get('/', (req, res) => {
            let db = new sqlite3.Database(LayersControllerV1.DB_NAME)

            db.all('SELECT * FROM layers WHERE include > 0', [], (err, rows) => {
                console.log(`Returned ${rows.length} rows`)
                res.json(rows)
            })
        })
    }
    
    getRouter(): IRouter {
        return this.router
    }
}
