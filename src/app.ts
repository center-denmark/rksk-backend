import { WebServer } from './web_server'

import { MunicipalitiesControllerV1 } from './controllers/v1/municipality-controller-v1'
import { WindTurbineControllerV1 } from './controllers/v1/windturbine-controller-v1'
import { PlantsControllerV1 } from './controllers/v1/plants-controller-v1'
import { LayersControllerV1 } from './controllers/v1/layers-controller-v1'
import { ObjectsControllerV1 } from './controllers/v1/objects-controller-v1'

const ws = new WebServer()

ws.register_api('/v1/layers', new LayersControllerV1())
ws.register_api('/v1/objects', new ObjectsControllerV1())

ws.register_api('/v1/municipalities', new MunicipalitiesControllerV1())
ws.register_api('/v1/windturbines', new WindTurbineControllerV1())
ws.register_api('/v1/plants', new PlantsControllerV1())

ws.listen()