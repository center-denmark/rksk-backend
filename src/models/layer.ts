export class Layer {
    constructor(
        public id: String,
        public name: String,
        public icon: String,
        public enabled: Boolean
    ) {}
}