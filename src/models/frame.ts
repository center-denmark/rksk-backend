export class Frame {
    constructor(
        public num: number,
        public consumption: number,
        public production: number,
        public timestamp: number,
        public temp: number,
        public wind_speed: number,
        public wind_direction: number,
        public co2: number
    ) {}
}