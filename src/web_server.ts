import express, { Router } from 'express'
import * as http from 'http'
import * as socketio from 'socket.io'
import cors from 'cors'
import { IController } from './interfaces/controller'
import * as fs from 'fs'
import csvp from 'csv-parse'
import { Frame } from './models/frame'

export class WebServer {
    private static readonly PORT = 4100

    private readonly app = express()
    private readonly server = new http.Server(this.app)
    private readonly io = new socketio.Server(this.server)
    private readonly api_router = Router()
    
    private datafeed: any[] = []

    constructor() {
        var parser = fs.createReadStream('data/rksk_sample.csv')
        .pipe(csvp({cast: true, from_line: 2}))
        .on('data', (row) => {
            this.datafeed.push(row)
        })
        .on('end', () => {
            console.log('Done reading CSV (%d frames)', this.datafeed.length)

            let index = 0
            let duration = 120 * 1000
            let sleep = duration  / this.datafeed.length
    
            console.log('Starting stream loop')

            var timer = setInterval(() => {
                
                    let row = this.datafeed[index++];
    
                    let frame = new Frame(row[0], row[1], row[2], new Date(row[3]).getTime(), row[4], row[5], row[6], row[7])   // Find better way to do this!
    
                    this.io.emit('frame', frame)
    
                    if (index >= this.datafeed.length) {
                        index = 0;
                    }
    
            }, sleep)
        })

        this.app.use(cors())

        this.app.use(express.static('public'))
        this.app.use('/api', this.api_router)

        this.io.on('connection', (socket) => {
            console.log('Someone connected!')
        })
    }

    register_api(path: string, controller: IController) {
        this.api_router.use(path, controller.getRouter())
    }

    listen() {
        this.server.listen(WebServer.PORT, () => {
            console.log(`Listening on ${WebServer.PORT}`)
        })
    }
}